ifneq ($(KERNELRELEASE),)

ccflags-y := -std=gnu99 -Wno-declaration-after-statement
obj-m := vintage2d.o
vintage2d-objs := v2d.o v2d_chrdev.o v2d_pci.o v2d_canvas.o

else

KDIR ?= /lib/modules/$(shell uname -r)/build

default:
	$(MAKE) -C $(KDIR) M=$$PWD

install:
	$(MAKE) -C $(KDIR) M=$$PWD modules_install

clean:
	$(MAKE) -C $(KDIR) M=$$PWD clean

test:
	modprobe vintage2d
	$(MAKE) -C z2_test/
	rmmod vintage2d

endif
