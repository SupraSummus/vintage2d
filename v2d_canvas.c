#include "v2d_canvas.h"
#include "vintage2d.h"
#include <linux/kernel.h>
#include <linux/pci.h>
#include <linux/vmalloc.h>

static const bool DEBUG = false;

#define MAX_PAGE_COUNT V2D_CANVAS_PAGE_COUNT(V2D_MAX_HEIGHT, V2D_MAX_WIDTH)

v2d_canvas_t * v2d_canvas_alloc (v2d_canvas_dim_t dim, struct device * dev) {
	if (DEBUG) printk(KERN_DEBUG "v2d_canvas: alloc\n");
	
	// alloc struct
	v2d_canvas_t * canvas = kmalloc(sizeof(v2d_canvas_t), GFP_KERNEL);
	if (canvas == NULL) return NULL;
	canvas->dim = dim;
	canvas->page_table = NULL;
	canvas->pages = NULL;
	
	// alloc page table
	canvas->page_table = dma_zalloc_coherent(dev, sizeof(uint32_t) * MAX_PAGE_COUNT, &canvas->page_table_dma, GFP_KERNEL);
	canvas->pages = vmalloc(sizeof(void *) * MAX_PAGE_COUNT);
	if (canvas->page_table == NULL || canvas->pages == NULL) {
		v2d_canvas_free(canvas, dev);
		return NULL;
	}
	memset(canvas->pages, 0, sizeof(void *) * MAX_PAGE_COUNT);
	
	// alloc canvas pages
	for (int i = 0; i < V2D_CANVAS_PAGE_COUNT(dim.width, dim.height); i ++) {
		canvas->pages[i] = dma_zalloc_coherent(dev, PAGE_SIZE, (dma_addr_t *)&canvas->page_table[i], GFP_KERNEL);
		if (canvas->pages[i] == NULL) {
			v2d_canvas_free(canvas, dev);
			return NULL;
		}
		canvas->page_table[i] |= VINTAGE2D_PTE_VALID; // mark as valid entry (for device)
	}
	
	if (DEBUG) printk(KERN_DEBUG "v2d_canvas: page table addr %p (dma %pad)\n", canvas->page_table, &canvas->page_table_dma);
	if (DEBUG) printk(KERN_DEBUG "v2d_canvas: total %lu pages\n", V2D_CANVAS_PAGE_COUNT(dim.width, dim.height));
	
	return canvas;
}

void v2d_canvas_free (v2d_canvas_t * canvas, struct device * dev) {
	if (DEBUG) printk(KERN_DEBUG "v2d_canvas: free\n");
	
	if (canvas == NULL) return;
	
	if (canvas->pages != NULL && canvas->page_table != NULL) {
		for (int i = 0; i < MAX_PAGE_COUNT; i ++) {
			if (canvas->pages[i] != NULL) {
				dma_free_coherent(dev, sizeof(uint32_t) * MAX_PAGE_COUNT, canvas->pages[i], canvas->page_table[i]);
			}
		}
	}
	
	vfree(canvas->pages);
	if (canvas->page_table != NULL) {
		dma_free_coherent(dev, sizeof(uint32_t) * MAX_PAGE_COUNT, canvas->page_table, canvas->page_table_dma);
	}
	
	canvas->pages = NULL;
	canvas->page_table = NULL;
} 

inline bool between(uint32_t lo, uint32_t hi, uint32_t x) {
	return lo <= x && x < hi;
}

bool v2d_verify_in_canvas(v2d_canvas_dim_t dim, uint32_t corner, uint32_t size) {
	return (
		between(0, dim.width, VINTAGE2D_CMD_POS_X(corner)) &&
		between(0, dim.height, VINTAGE2D_CMD_POS_Y(corner)) &&
		between(0, dim.width, VINTAGE2D_CMD_POS_X(corner) + VINTAGE2D_CMD_WIDTH(size) - 1) &&
		between(0, dim.height, VINTAGE2D_CMD_POS_Y(corner) + VINTAGE2D_CMD_HEIGHT(size) - 1)
	);
}
