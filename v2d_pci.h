#ifndef V2D_PCI_H
#define V2D_PCI_H

#include "v2d_chrdev.h"
#include "v2d_canvas.h"
#include <linux/types.h>
#include <linux/list.h>

typedef struct _v2d_pci_device_t v2d_pci_device_t;

extern v2d_pci_device_t * v2d_pci_get(int minor);
extern bool v2d_pci_add_context(v2d_pci_device_t *, struct list_head * context);
extern void v2d_pci_remove_context(v2d_pci_device_t *, struct list_head * context);

// blocking
extern bool v2d_pci_exec(
	v2d_pci_device_t * dev, v2d_canvas_t *,
	uint32_t, uint32_t, uint32_t
);
extern bool v2d_pci_sync(v2d_pci_device_t * dev);
extern void v2d_pci_invalidate_canvas(v2d_pci_device_t * dev, v2d_canvas_t *);

extern struct pci_dev * v2d_pci_get_device(v2d_pci_device_t * dev);

extern bool v2d_pci_init(void);
extern void v2d_pci_cleanup(void);

#endif
