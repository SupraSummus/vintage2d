#include "v2d_pci.h"
#include "v2d_chrdev.h"
#include "vintage2d.h"
#include <linux/pci.h>
#include <linux/kernel.h>
#include <linux/kdev_t.h>
#include <linux/spinlock.h>
#include <linux/interrupt.h>
#include <linux/semaphore.h>

static const bool DEBUG = true;
static const bool MUCH_DEBUG = false;

#define CMD_QUEUE_SIZE (PAGE_SIZE / sizeof(uint32_t))
#define NOTIFY_EVERY (128)

#define COUNTER_SYNC (42)
#define COUNTER_NOTIFY (1)

// ### structs ###

struct _v2d_pci_device_t {
	// pci device data
	spinlock_t irq_lock;
	void __iomem * mem;
	struct pci_dev * pci_dev;
	bool handler_registered;
	
	// chr device connected with this pci device
	int minor;
	struct device * chrdev;
	
	// collection of user contexts
	spinlock_t contexts_lock;
	bool contexts_allowed;
	struct list_head contexts;
	
	// cmd queue
	struct mutex queue_lock;
	v2d_canvas_t * canvas; // context after last cmd in queue
	int first_free_position;
	int last_free_position;
	int cmds_since_notify;
	struct semaphore queue_free;
		// state of this semaphore is number of free positions in queue
	struct semaphore queue_sync;
		// semaphore for waiting to empty queue
	uint32_t * cmd_queue;
	dma_addr_t cmd_queue_dma;
};

// ### static vars ###

#define max_minor_count (256)
static v2d_pci_device_t * minors[max_minor_count];
static spinlock_t minors_lock;

// ### private functions decls ###

static void v2d_remove(struct pci_dev *);
static void register_dump(v2d_pci_device_t *);

v2d_pci_device_t * v2d_pci_get(int minor) {
	spin_lock(&minors_lock);
	
	v2d_pci_device_t * dev = NULL;
	if (minor < 0 || minor >= max_minor_count) {
		dev = NULL;
	} else {
		dev = minors[minor];
	}
	
	spin_unlock(&minors_lock);
	return dev;
}

bool v2d_pci_add_context(v2d_pci_device_t * pdev, struct list_head * context) {
	bool ret;
	spin_lock(&pdev->contexts_lock);
	
	ret = pdev->contexts_allowed;
	if (pdev->contexts_allowed) {
		list_add_tail(context, &pdev->contexts);
	}
	
	spin_unlock(&pdev->contexts_lock);
	return ret;
}

void v2d_pci_remove_context(v2d_pci_device_t * pdev, struct list_head * context) {
	spin_lock(&pdev->contexts_lock);
	
	if (pdev->contexts_allowed) {
		list_del(context);
	}
	
	spin_unlock(&pdev->contexts_lock);
}

// read queue start ptr from device
static void sync_queue_start (v2d_pci_device_t * dev) {
	unsigned long flags;
	spin_lock_irqsave(&dev->irq_lock, flags);
	
	uint32_t read_ptr = ioread32(dev->mem + VINTAGE2D_CMD_READ_PTR);
	int last_free_position = (read_ptr == dev->cmd_queue_dma) ?
		CMD_QUEUE_SIZE - 1 :
		(read_ptr - dev->cmd_queue_dma) / sizeof(uint32_t) - 1;
	if (MUCH_DEBUG) printk(KERN_DEBUG "v2d_pci: read last free position %d (current %d)\n", last_free_position, dev->last_free_position);
	
	while (last_free_position != dev->last_free_position) {
		up(&dev->queue_free);
		dev->last_free_position ++;
		if (dev->last_free_position == CMD_QUEUE_SIZE) {
			dev->last_free_position = 0;
		}
	}
	
	spin_unlock_irqrestore(&dev->irq_lock, flags);
}

// write queue end ptr to device
static void sync_queue_end (v2d_pci_device_t * dev) {
	if (MUCH_DEBUG) printk(KERN_DEBUG "v2d_pci: writing queue end to device %x (start %x - stop %x)\n",
		dev->first_free_position * sizeof(uint32_t) + (uint32_t)dev->cmd_queue_dma,
		dev->last_free_position * sizeof(uint32_t) + (uint32_t)dev->cmd_queue_dma,
		dev->first_free_position * sizeof(uint32_t) + (uint32_t)dev->cmd_queue_dma
	);
	//unsigned long flags;
	//spin_lock_irqsave(&dev->irq_lock, flags);
	
	iowrite32(dev->first_free_position * sizeof(uint32_t) + (uint32_t)dev->cmd_queue_dma, dev->mem + VINTAGE2D_CMD_WRITE_PTR);
	
	//spin_unlock_irqrestore(&dev->irq_lock, flags);
}

static bool v2d_pci_exec_raw(v2d_pci_device_t * dev, uint32_t cmd) {
	if (dev->first_free_position == CMD_QUEUE_SIZE - 1) {
		// we are at the end of a buffer
		if (down_interruptible(&dev->queue_free) != 0) {
			return false;
		}
		dev->cmd_queue[dev->first_free_position] = VINTAGE2D_CMD_JUMP(dev->cmd_queue_dma);
		dev->first_free_position = 0;
	}
	
	// insert cmd
	if (down_interruptible(&dev->queue_free) != 0) {
		return false;
	}
	dev->cmd_queue[dev->first_free_position] = cmd;
	dev->first_free_position ++;
	dev->cmds_since_notify ++;
	
	return true;
}

static bool v2d_pci_sync_raw(v2d_pci_device_t * dev) {
	if (
		// sometimes second exec doesnt trigger intr???
		!v2d_pci_exec_raw(dev, VINTAGE2D_CMD_COUNTER(COUNTER_SYNC, false)) ||
		!v2d_pci_exec_raw(dev, VINTAGE2D_CMD_COUNTER(COUNTER_SYNC, true))
	) {
		return false;
	}
	dev->cmds_since_notify = 0;
	sync_queue_end(dev);
	down(&dev->queue_sync); // must "consume" generated intr
	return true;
}

bool v2d_pci_exec(v2d_pci_device_t * dev, v2d_canvas_t * canvas, uint32_t a, uint32_t b, uint32_t c) {
	if (mutex_lock_interruptible(&dev->queue_lock) != 0) {
		return false;
	}
	
	// return if device is shutting down
	spin_lock(&dev->contexts_lock);
	if (!dev->contexts_allowed) {
		spin_unlock(&dev->contexts_lock);
		mutex_unlock(&dev->queue_lock);
		return false;
	}
	spin_unlock(&dev->contexts_lock);
	
	// set page table if it has changed
	if (canvas != dev->canvas) {
		if (DEBUG) printk(KERN_DEBUG "v2d_pci: switching contexts from %p to %p\n", dev->canvas, canvas);
		
		// set page table
		if (!v2d_pci_exec_raw(dev, VINTAGE2D_CMD_CANVAS_PT(canvas->page_table_dma, false))) {
			mutex_unlock(&dev->queue_lock);
			return false;
		}
		dev->canvas = NULL; // unsynchronized
		
		// synchronously reset device tlb
		if (!v2d_pci_sync_raw(dev)) {
			mutex_unlock(&dev->queue_lock);
			return false;
		}
		iowrite32(VINTAGE2D_RESET_TLB, dev->mem + VINTAGE2D_RESET);
		
		// set canvas dimensions
		if (!v2d_pci_exec_raw(dev, VINTAGE2D_CMD_CANVAS_DIMS(canvas->dim.width, canvas->dim.height, false))) {
			mutex_unlock(&dev->queue_lock);
			return false;
		}
		
		dev->canvas = canvas;
	}
	
	if (
		!v2d_pci_exec_raw(dev, a) ||
		!v2d_pci_exec_raw(dev, b) ||
		!v2d_pci_exec_raw(dev, c)
	) {
		mutex_unlock(&dev->queue_lock);
		return false;
	}
	
	// interlace with notifies
	if (dev->cmds_since_notify >= NOTIFY_EVERY) {
		dev->cmds_since_notify = 0;
		v2d_pci_exec_raw(dev, VINTAGE2D_CMD_COUNTER(COUNTER_NOTIFY, true));
	}
	
	sync_queue_end(dev);
	
	mutex_unlock(&dev->queue_lock);
	return true;
}

bool v2d_pci_sync(v2d_pci_device_t * dev) {
	if (mutex_lock_interruptible(&dev->queue_lock) != 0) {
		return false;
	}
	
	// return if device is shutting down
	spin_lock(&dev->contexts_lock);
	if (!dev->contexts_allowed) {
		spin_unlock(&dev->contexts_lock);
		mutex_unlock(&dev->queue_lock);
		return false;
	}
	spin_unlock(&dev->contexts_lock);
	
	bool ret = v2d_pci_sync_raw(dev);
	
	mutex_unlock(&dev->queue_lock);
	return ret;
}

void v2d_pci_invalidate_canvas(v2d_pci_device_t * dev, v2d_canvas_t * canvas) {
	mutex_lock(&dev->queue_lock);
	
	// return if device is shutting down
	spin_lock(&dev->contexts_lock);
	if (!dev->contexts_allowed) {
		spin_unlock(&dev->contexts_lock);
		mutex_unlock(&dev->queue_lock);
		return;
	}
	spin_unlock(&dev->contexts_lock);
	
	if (dev->canvas == canvas) {
		dev->canvas = NULL;
		if (DEBUG) printk(KERN_DEBUG "v2d_pci: invalidate context %p\n", canvas);
	}
	
	mutex_unlock(&dev->queue_lock);
}

struct pci_dev * v2d_pci_get_device(v2d_pci_device_t * dev) {
	return dev->pci_dev;
}

static void free_minor(int minor) {
	spin_lock(&minors_lock);
	
	if (minor >= 0 && minor < max_minor_count) {
		minors[minor] = NULL;
	}
	
	spin_unlock(&minors_lock);
}

static int alloc_minor(v2d_pci_device_t * dev) {
	spin_lock(&minors_lock);
	
	int i = 0;
	while (i < max_minor_count && minors[i] != NULL) i++;
	if (i >= max_minor_count) {
		spin_unlock(&minors_lock);
		return -1;
	} else {
		minors[i] = dev;
		spin_unlock(&minors_lock);
		return i;
	}
}

static irqreturn_t v2d_irq_handler (int irq, void * _dev) {
	v2d_pci_device_t * dev = _dev;
	unsigned int intr = ioread32(dev->mem + VINTAGE2D_INTR);
	
	if (intr & VINTAGE2D_INTR_CANVAS_OVERFLOW) {
		iowrite32(VINTAGE2D_ENABLE_DRAW, dev->mem + VINTAGE2D_ENABLE); // skip this cmd
		iowrite32(VINTAGE2D_INTR_CANVAS_OVERFLOW, dev->mem + VINTAGE2D_INTR);
		printk(KERN_WARNING "v2d_pci: canvas overflow\n");
		register_dump(dev);
		return IRQ_HANDLED;
	}
	
	if (intr & VINTAGE2D_INTR_FIFO_OVERFLOW) {
		iowrite32(VINTAGE2D_INTR_FIFO_OVERFLOW, dev->mem + VINTAGE2D_INTR);
		printk(KERN_WARNING "v2d_pci: fifo overflow\n");
		register_dump(dev);
		return IRQ_HANDLED;
	}
	
	if (intr & VINTAGE2D_INTR_INVALID_CMD) {
		iowrite32(VINTAGE2D_INTR_INVALID_CMD, dev->mem + VINTAGE2D_INTR);
		printk(KERN_WARNING "v2d_pci: invalid cmd\n");
		register_dump(dev);
		return IRQ_HANDLED;
	}
	
	if (intr & VINTAGE2D_INTR_NOTIFY) {
		uint32_t counter = ioread32(dev->mem + VINTAGE2D_COUNTER);
		if (MUCH_DEBUG) printk(KERN_DEBUG "v2d_pci: intr notify %d\n", counter);
		
		switch (counter) {
			case COUNTER_SYNC:
				up(&dev->queue_sync);
			case COUNTER_NOTIFY:
				sync_queue_start(dev);
				break;
			default:
				printk(KERN_WARNING "v2d_pci: invalid notify intr\n");
				register_dump(dev);
		}
		
		iowrite32(VINTAGE2D_INTR_NOTIFY, dev->mem + VINTAGE2D_INTR);
		return IRQ_HANDLED;
	}
	
	if (intr & VINTAGE2D_INTR_PAGE_FAULT) {
		iowrite32(VINTAGE2D_INTR_PAGE_FAULT, dev->mem + VINTAGE2D_INTR);
		printk(KERN_WARNING "v2d_pci: page fault\n");
		register_dump(dev);
		return IRQ_HANDLED;
	}
	
	return IRQ_NONE;
}

static int v2d_probe(struct pci_dev * dev, const struct pci_device_id * id) {
	if (DEBUG) printk(KERN_DEBUG "v2d_pci: new device\n");
	
	// create and register private device data
	v2d_pci_device_t * dev_data = kmalloc(sizeof(v2d_pci_device_t), GFP_KERNEL);
	if (dev_data == NULL) {
		printk(KERN_ERR "v2d_pci: failed to alloc device data\n");
		return -EFAULT;
	}
	pci_set_drvdata(dev, dev_data);
	*dev_data = (v2d_pci_device_t) {
		mem: NULL,
		pci_dev: dev,
		handler_registered: false,
		minor: -1,
		chrdev: NULL,
		contexts_allowed: true,
		canvas: NULL,
		first_free_position: 0,
		last_free_position: CMD_QUEUE_SIZE - 1,
		cmds_since_notify: 0,
		cmd_queue: NULL,
	};
	spin_lock_init(&dev_data->irq_lock);
	spin_lock_init(&dev_data->contexts_lock);
	INIT_LIST_HEAD(&dev_data->contexts);
	mutex_init(&dev_data->queue_lock);
	sema_init(&dev_data->queue_free, CMD_QUEUE_SIZE - 1);
	sema_init(&dev_data->queue_sync, 0);
	
	// enable device
	if (
		pci_enable_device(dev) != 0 ||
		pci_request_regions(dev, "v2d_pci") != 0
	) {
		printk(KERN_ERR "v2d_pci: failed to enable pci device\n");
		v2d_remove(dev);
		return -EFAULT;
	}
	
	// request io-mem mapping
	dev_data->mem = pci_iomap(dev, 0, 4*1024);
	if (dev_data->mem == NULL) {
		printk(KERN_ERR "v2d_pci: failed to pci_iomap\n");
		v2d_remove(dev);
		return -EFAULT;
	}
	iowrite32(0, dev_data->mem + VINTAGE2D_ENABLE); // disable device
	iowrite32(0, dev_data->mem + VINTAGE2D_INTR_ENABLE); // disable intrs
	iowrite32( // clear all interrupts
		VINTAGE2D_INTR_CANVAS_OVERFLOW |
		VINTAGE2D_INTR_FIFO_OVERFLOW |
		VINTAGE2D_INTR_INVALID_CMD |
		VINTAGE2D_INTR_NOTIFY |
		VINTAGE2D_INTR_PAGE_FAULT,
		dev_data->mem + VINTAGE2D_INTR
	);
	
	// enable dma
	pci_set_master(dev);
	pci_set_dma_mask(dev, DMA_BIT_MASK(32)); // TODO check result
	pci_set_consistent_dma_mask(dev, DMA_BIT_MASK(32)); // TODO check result
	
	// alloc cmd queue in dma zone
	dev_data->cmd_queue = dma_alloc_coherent(
		&dev->dev, sizeof(uint32_t) * CMD_QUEUE_SIZE,
		&dev_data->cmd_queue_dma, GFP_KERNEL
	);
	if (dev_data->cmd_queue == NULL) {
		printk(KERN_ERR "v2d_pci: failed to dma-alloc cmd queue\n");
		v2d_remove(dev);
		return -ENOMEM;
	}
	if (DEBUG) printk(KERN_DEBUG "v2d_pci: cmd queue allocated at %p (dma %pad)\n",
		dev_data->cmd_queue, &dev_data->cmd_queue_dma);
	
	// regiser interrupt handler
	if (request_irq(dev->irq, v2d_irq_handler, IRQF_SHARED, "v2d_irq", dev_data) != 0) {
		printk(KERN_ERR "v2d_pci: failed to register irq\n");
		dev_data->handler_registered = false;
		v2d_remove(dev);
		return -EFAULT;
	} else {
		dev_data->handler_registered = true;
	}
	
	// configure device
	iowrite32( // reset all
		VINTAGE2D_RESET_DRAW | VINTAGE2D_RESET_FIFO | VINTAGE2D_RESET_TLB,
		dev_data->mem + VINTAGE2D_RESET
	);
	iowrite32(dev_data->cmd_queue_dma, dev_data->mem + VINTAGE2D_CMD_READ_PTR); // next cmd to load
	iowrite32(dev_data->cmd_queue_dma, dev_data->mem + VINTAGE2D_CMD_WRITE_PTR); // stop load at
	iowrite32( // enable all interrupts
		VINTAGE2D_INTR_CANVAS_OVERFLOW |
		VINTAGE2D_INTR_FIFO_OVERFLOW |
		VINTAGE2D_INTR_INVALID_CMD |
		VINTAGE2D_INTR_NOTIFY |
		VINTAGE2D_INTR_PAGE_FAULT,
		dev_data->mem + VINTAGE2D_INTR_ENABLE
	);
	iowrite32( // enable all
		VINTAGE2D_ENABLE_DRAW | VINTAGE2D_ENABLE_FETCH_CMD,
		dev_data->mem + VINTAGE2D_ENABLE
	);
	//register_dump(dev_data);
	//iowrite32(VINTAGE2D_CMD_COUNTER(50, true), dev_data->mem + VINTAGE2D_FIFO_SEND);
	
	// make sysfs node
	if (v2d_chrdev_major > 0 && v2d_chrdev_class != NULL) {
		// only if chrdevice is set up
		
		dev_data->minor = alloc_minor(dev_data);
		if (dev_data->minor == -1) {
			printk(KERN_ERR "v2d_pci: no free minor number for device\n");
			v2d_remove(dev);
			return -EFAULT;
		}
		
		dev_data->chrdev = device_create(
			v2d_chrdev_class, &dev->dev,
			MKDEV(v2d_chrdev_major, dev_data->minor), NULL,
			"v2d%d", dev_data->minor
		);
		if ((void *)dev_data->chrdev == ERR_PTR) {
			printk(KERN_ERR "v2d_pci: failed to create device (v2d%d)\n", dev_data->minor);
			free_minor(dev_data->minor);
			dev_data->minor = -1;
			dev_data->chrdev = NULL;
			v2d_remove(dev);
			return -EFAULT;
		}
	}
	
	return 0;
}

static void v2d_remove(struct pci_dev * dev) {
	if (DEBUG) printk(KERN_DEBUG "v2d_pci: remove device\n");
	
	unsigned long flags;
	v2d_pci_device_t * dev_data = pci_get_drvdata(dev);
	pci_set_drvdata(dev, NULL);
	if (dev_data == NULL) return;
	
	// release character device
	if (dev_data->minor >= 0) {
		device_destroy(v2d_chrdev_class, MKDEV(v2d_chrdev_major, dev_data->minor));
		free_minor(dev_data->minor);
		dev_data->chrdev = NULL;
		dev_data->minor = -1;
	}
	
	// disable device
	if (dev_data->mem != NULL) {
		// only if we have mapped io
		spin_lock_irqsave(&dev_data->irq_lock, flags);
		iowrite32(0, dev_data->mem + VINTAGE2D_INTR_ENABLE); // disable intrs
		iowrite32(0, dev_data->mem + VINTAGE2D_ENABLE); // disable all
		spin_unlock_irqrestore(&dev_data->irq_lock, flags);
	}
	
	// unregister irq
	if (dev_data->handler_registered) {
		free_irq(dev->irq, dev_data);
		dev_data->handler_registered = false;
	}
	
	// finalize contexts
	struct list_head * i, * next;
	spin_lock(&dev_data->contexts_lock);
	dev_data->contexts_allowed = false;
	spin_unlock(&dev_data->contexts_lock);
	up(&dev_data->queue_sync); // release process waiting for empty queue
	for (int j = 0; j < 10; j ++) {
		// release process waiting for free space in queue
		// assume 10 free positions will be enough
		up(&dev_data->queue_free);
	}
	list_for_each_safe(i, next, &dev_data->contexts) {
		v2d_chrdev_finalize_context(i);
	}
	
	// free queue
	dma_free_coherent(
		&dev->dev, sizeof(uint32_t) * CMD_QUEUE_SIZE,
		dev_data->cmd_queue, dev_data->cmd_queue_dma
	);
	
	// disable dma
	pci_clear_master(dev);
	
	// unmap io
	if (dev_data->mem != NULL) {
		pci_iounmap(dev, dev_data->mem);
		dev_data->mem = NULL;
	}
	
	// disable device
	pci_release_regions(dev);
	pci_disable_device(dev);
	
	// free data
	kfree(dev_data);
}

/*static void v2d_shutdown(struct pci_dev * dev) {
}*/

static const struct pci_device_id v2d_id[2] = {{PCI_DEVICE(VINTAGE2D_VENDOR_ID, VINTAGE2D_DEVICE_ID)},{}};

static struct pci_driver v2d_pci_driver = {
	name: "v2d_pci",
	id_table: v2d_id,
	probe: v2d_probe,
	remove: v2d_remove,
//	shutdown: v2d_shutdown,
};

bool v2d_pci_init (void) {
	if (DEBUG) printk(KERN_DEBUG "v2d_pci: init\n");
	
	spin_lock_init(&minors_lock);
	
	if (pci_register_driver(&v2d_pci_driver) != 0) {
		printk(KERN_ERR "v2d_pci: failed to register driver\n");
		return false;
	}
	
	return true;
}

void v2d_pci_cleanup (void) {
	if (DEBUG) printk(KERN_DEBUG "v2d_pci: cleanup\n");
	
	pci_unregister_driver(&v2d_pci_driver);
}

static void register_dump(v2d_pci_device_t * dev) {
	printk(KERN_DEBUG "v2d_pci: register dump:\n");
	printk(KERN_DEBUG "v2d_pci: ==============\n");
	
	uint32_t enable = ioread32(dev->mem + VINTAGE2D_ENABLE);
	printk(KERN_DEBUG "v2d_pci:   ENABLE:         %x\n", enable);
	printk(KERN_DEBUG "v2d_pci:       DRAW            %d\n", (enable & VINTAGE2D_ENABLE_DRAW) ? 1 : 0);
	printk(KERN_DEBUG "v2d_pci:       FETCH_CMD       %d\n", (enable & VINTAGE2D_ENABLE_FETCH_CMD) ? 1 : 0);
	
	uint32_t status = ioread32(dev->mem + VINTAGE2D_STATUS);
	printk(KERN_DEBUG "v2d_pci:   STATUS:         %x\n", status);
	printk(KERN_DEBUG "v2d_pci:       DRAW            %d\n", (status & VINTAGE2D_STATUS_DRAW) ? 1 : 0);
	printk(KERN_DEBUG "v2d_pci:       FIFO            %d\n", (status & VINTAGE2D_STATUS_FIFO) ? 1 : 0);
	printk(KERN_DEBUG "v2d_pci:       FETCH_CMD       %d\n", (status & VINTAGE2D_STATUS_FETCH_CMD) ? 1 : 0);
	
	uint32_t intr = ioread32(dev->mem + VINTAGE2D_INTR);
	uint32_t intr_enable = ioread32(dev->mem + VINTAGE2D_INTR_ENABLE);
	printk(KERN_DEBUG "v2d_pci:   INTR:           %x\n", intr);
	printk(KERN_DEBUG "v2d_pci:   INTR_ENABLE:    %x\n", intr_enable);
	printk(KERN_DEBUG "v2d_pci:                   active  enabled\n");
	printk(KERN_DEBUG "v2d_pci:       NOTIFY          %d  %d\n", (intr & VINTAGE2D_INTR_NOTIFY) ? 1 : 0, (intr_enable & VINTAGE2D_INTR_NOTIFY) ? 1 : 0);
	printk(KERN_DEBUG "v2d_pci:       INVALID_CMD     %d  %d\n", (intr & VINTAGE2D_INTR_INVALID_CMD) ? 1 : 0, (intr_enable & VINTAGE2D_INTR_INVALID_CMD) ? 1 : 0);
	printk(KERN_DEBUG "v2d_pci:       PAGE_FAULT      %d  %d\n", (intr & VINTAGE2D_INTR_PAGE_FAULT) ? 1 : 0, (intr_enable & VINTAGE2D_INTR_PAGE_FAULT) ? 1 : 0);
	printk(KERN_DEBUG "v2d_pci:       CANVAS_OVERFLOW %d  %d\n", (intr & VINTAGE2D_INTR_CANVAS_OVERFLOW) ? 1 : 0, (intr_enable & VINTAGE2D_INTR_CANVAS_OVERFLOW) ? 1 : 0);
	printk(KERN_DEBUG "v2d_pci:       FIFO_OVERFLOW   %d  %d\n", (intr & VINTAGE2D_INTR_FIFO_OVERFLOW) ? 1 : 0, (intr_enable & VINTAGE2D_INTR_FIFO_OVERFLOW) ? 1 : 0);
	
	printk(KERN_DEBUG "v2d_pci:   FIFO_FREE:      %x\n", ioread32(dev->mem + VINTAGE2D_FIFO_FREE));
	printk(KERN_DEBUG "v2d_pci:   COUNTER:        %x\n", ioread32(dev->mem + VINTAGE2D_COUNTER));
	printk(KERN_DEBUG "v2d_pci:   CMD_READ_PTR:   %x\n", ioread32(dev->mem + VINTAGE2D_CMD_READ_PTR));
	printk(KERN_DEBUG "v2d_pci:   CMD_WRITE_PTR:  %x\n", ioread32(dev->mem + VINTAGE2D_CMD_WRITE_PTR));
}
