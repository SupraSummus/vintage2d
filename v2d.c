#include "v2d.h"
#include "v2d_pci.h"
#include "v2d_chrdev.h"

#include <linux/module.h>
#include <linux/types.h>
#include <linux/kernel.h>
#include <linux/init.h>

static const bool DEBUG = true;

static int v2d_init(void) {
	if (DEBUG) printk(KERN_DEBUG "v2d: init\n");
	v2d_chrdev_init();
	v2d_pci_init();
	return 0;
}

static void v2d_cleanup(void) {
	v2d_pci_cleanup();
	v2d_chrdev_cleanup();
	if (DEBUG) printk(KERN_DEBUG "v2d: exit\n");
}

module_init(v2d_init);
module_exit(v2d_cleanup);

MODULE_DESCRIPTION("vintage2d driver");
MODULE_AUTHOR("Jan Rydzewski");
MODULE_LICENSE("GPL v2");
