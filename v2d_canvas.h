#ifndef V2D_CANVAS
#define V2D_CANVAS

#include <linux/kernel.h>
#include <linux/pci.h>
#include "v2d_ioctl.h"

#define V2D_CANVAS_PAGE_COUNT(__width, __height) DIV_ROUND_UP(__width * __height, PAGE_SIZE)

typedef struct v2d_ioctl_set_dimensions v2d_canvas_dim_t;
typedef struct {
	v2d_canvas_dim_t dim;
	uint32_t * page_table; // ptr to array (page) of dma addrs of canvas pages
	dma_addr_t page_table_dma; // dma handle of page_table
	void ** pages; // ptr to array (page) of ptrs to canvas pages
} v2d_canvas_t;

extern v2d_canvas_t * v2d_canvas_alloc(v2d_canvas_dim_t, struct device * dev);
extern void v2d_canvas_free(v2d_canvas_t *, struct device * dev);

extern bool v2d_verify_in_canvas(v2d_canvas_dim_t, uint32_t corner, uint32_t size);

#endif
