#include "v2d_chrdev.h"
#include "v2d_pci.h"
#include "v2d_ioctl.h"
#include "v2d_canvas.h"
#include "vintage2d.h"
#include <linux/types.h>
#include <linux/kernel.h>
#include <linux/fs.h>
#include <linux/device.h>
#include <linux/slab.h>
#include <asm-generic/uaccess.h>
#include <linux/mutex.h>
#include <linux/mm.h>

struct _v2d_context_t {
	struct mutex lock;
	struct mutex ioctl_lock;
	
	struct list_head contexts;
	v2d_pci_device_t * device;
	v2d_canvas_t * canvas;
	
	uint32_t src;
	uint32_t dst;
	uint32_t color;
};

static const bool DEBUG = true;
static const char * CHRDEV_NAME = "v2d_chrdev";

static int v2d_release(struct inode * inode, struct file * file);

void v2d_chrdev_finalize_context(struct list_head * context_head) {
	if (DEBUG) printk(KERN_DEBUG "v2d_chrdev: finalize_context\n");
	
	v2d_context_t * context = list_entry(context_head, v2d_context_t, contexts);
	
	mutex_lock(&context->ioctl_lock);
	mutex_lock(&context->lock);
	
	if (context->canvas != NULL) {
		v2d_canvas_free(context->canvas, &v2d_pci_get_device(context->device)->dev);
	}
	context->canvas = NULL;
	context->device = NULL;
	
	mutex_unlock(&context->lock);
	mutex_unlock(&context->ioctl_lock);
};

static int v2d_open (struct inode * inode, struct file * file) {
	if (DEBUG) printk(KERN_DEBUG "v2d_chrdev: open\n");
	
	v2d_context_t * context = kmalloc(sizeof(v2d_context_t), GFP_KERNEL);
	if (context == NULL) {
		return -ENOMEM;
	}
	file->private_data = context;
	mutex_init(&context->lock);
	mutex_init(&context->ioctl_lock);
	INIT_LIST_HEAD(&context->contexts);
	context->device = NULL;
	context->canvas = NULL;
	context->src = VINTAGE2D_CMD_TYPE_INVALID;
	context->dst = VINTAGE2D_CMD_TYPE_INVALID;
	context->color = VINTAGE2D_CMD_TYPE_INVALID;
	
	// get device
	context->device = v2d_pci_get(MINOR(inode->i_rdev));
	if (context->device == NULL) {
		printk(KERN_WARNING "v2d_chrdev: null device for minor %d\n", MINOR(inode->i_rdev));
		v2d_release(inode, file);
		return -EFAULT;
	}
	
	// register context
	if (!v2d_pci_add_context(context->device, &context->contexts)) {
		v2d_release(inode, file);
		return -EFAULT;
	}
	
	return 0;
}

static int v2d_release (struct inode * inode, struct file * file) {
	if (DEBUG) printk(KERN_DEBUG "v2d_chrdev: release\n");
	
	v2d_context_t * context = file->private_data;
	file->private_data = NULL;
	if (context == NULL) return 0;
	
	mutex_lock(&context->ioctl_lock);
	mutex_lock(&context->lock);
	
	// make sure dma is not using buffers
	if (context->device != NULL) {
		if (!v2d_pci_sync(context->device)) {
			printk(KERN_WARNING "v2d_pci: failed to sync at release\n");
		}
		// and invalidate page table if needed
		v2d_pci_invalidate_canvas(context->device, context->canvas);
	}
	
	// free canvas
	if (context->canvas != NULL) {
		v2d_canvas_free(context->canvas, &v2d_pci_get_device(context->device)->dev);
	}
	context->canvas = NULL;
	
	// unregister context
	if (context->device != NULL && !list_empty(&context->contexts)) {
		v2d_pci_remove_context(context->device, &context->contexts);
	}
	context->device = NULL;
	
	mutex_unlock(&context->lock);
	mutex_unlock(&context->ioctl_lock);
	
	// free data
	kfree(context);
	
	return 0;
}

static ssize_t v2d_write (struct file * file, const char __user * data_ptr, size_t size, loff_t * off) {
	v2d_context_t * context = file->private_data;
	int cmd_size = sizeof(uint32_t);
	if (context == NULL) return -EFAULT;
	if (size % cmd_size != 0) return -EINVAL;
	int ret;
	
	ssize_t written = 0;
	
	while (size >= cmd_size) {
		uint32_t cmd, cmd2;
		if (copy_from_user(&cmd, data_ptr, cmd_size) != 0) {
			return -EFAULT;
		}
		
		if ((ret = mutex_lock_interruptible(&context->lock)) != 0) {
			return ret;
		}
		
		if (context->device == NULL) {
			mutex_unlock(&context->lock);
			return -EFAULT;
		}
		if (context->canvas == NULL) {
			mutex_unlock(&context->lock);
			return -EINVAL;
		}
		
		switch (V2D_CMD_TYPE(cmd)) {
			
			case V2D_CMD_TYPE_SRC_POS:
				cmd2 = VINTAGE2D_CMD_SRC_POS(
					V2D_CMD_POS_X(cmd),
					V2D_CMD_POS_Y(cmd),
					false
				);
				if (
					cmd != cmd2 || // no unspecified bits set
					!v2d_verify_in_canvas( // point in canvas
						context->canvas->dim, 
						cmd, VINTAGE2D_CMD_DO_BLIT(1, 1, false)
					)
				) goto invalid_cmd;
				context->src = cmd2;
				break;
			
			case V2D_CMD_TYPE_DST_POS:
				cmd2 = VINTAGE2D_CMD_DST_POS(
					V2D_CMD_POS_X(cmd),
					V2D_CMD_POS_Y(cmd),
					false
				);
				if (
					cmd != cmd2 || // no unspecified bits set
					!v2d_verify_in_canvas( // point in canvas
						context->canvas->dim, 
						cmd, VINTAGE2D_CMD_DO_BLIT(1, 1, false)
					)
				) goto invalid_cmd;
				context->dst = cmd2;
				break;
			
			case V2D_CMD_TYPE_FILL_COLOR:
				cmd2 = VINTAGE2D_CMD_FILL_COLOR(
					V2D_CMD_COLOR(cmd),
					false
				);
				if (cmd != cmd2) goto invalid_cmd; // no unspecified bits set
				context->color = cmd2;
				break;
			
			case V2D_CMD_TYPE_DO_BLIT:
				cmd2 = VINTAGE2D_CMD_DO_BLIT(
					V2D_CMD_WIDTH(cmd),
					V2D_CMD_HEIGHT(cmd),
					false
				);
				
				if (
					cmd != cmd2 || // no unspecified bits
					context->src == VINTAGE2D_CMD_TYPE_INVALID || // src unsepcified
					context->dst == VINTAGE2D_CMD_TYPE_INVALID || // dst unsepcified
					!v2d_verify_in_canvas( // src rect in canvas
						context->canvas->dim,
						context->src, cmd2
					) ||
					!v2d_verify_in_canvas( // dst rect in canvas
						context->canvas->dim,
						context->dst, cmd2
					)
				) goto invalid_cmd;
				
				if (!v2d_pci_exec(
					context->device, context->canvas,
					context->src, context->dst, cmd
				)) {
					mutex_unlock(&context->lock);
					return written;
				}
				context->src = VINTAGE2D_CMD_TYPE_INVALID;
				context->dst = VINTAGE2D_CMD_TYPE_INVALID; // clear stored
				break;
			
			case V2D_CMD_TYPE_DO_FILL:
				cmd2 = VINTAGE2D_CMD_DO_FILL(
					V2D_CMD_WIDTH(cmd),
					V2D_CMD_HEIGHT(cmd),
					false
				);
				
				if (
					cmd != cmd2 || // no unspecified bits set
					context->dst == VINTAGE2D_CMD_TYPE_INVALID || // dst unsepcified
					context->color == VINTAGE2D_CMD_TYPE_INVALID || // color unsepcified
					!v2d_verify_in_canvas( // dst rect in canvas
						context->canvas->dim,
						context->dst, cmd2
					)
				) goto invalid_cmd;
				
				if (!v2d_pci_exec(
					context->device, context->canvas,
					context->dst, context->color, cmd
				)) {
					mutex_unlock(&context->lock);
					return written;
				}
				context->dst = VINTAGE2D_CMD_TYPE_INVALID; // clear stored
				context->color = VINTAGE2D_CMD_TYPE_INVALID;
				break;
			
			default:
				invalid_cmd:
				mutex_unlock(&context->lock);
				return -EINVAL;
		}
		
		mutex_unlock(&context->lock);
		
		written += cmd_size;
		size -= cmd_size;
		data_ptr += cmd_size;
		*off += cmd_size;
	}
	
	return written;
}

static long v2d_ioctl (struct file * file, unsigned int cmd, unsigned long arg) {
	int ret;
	v2d_context_t * context = file->private_data;
	if (context == NULL) return -EFAULT;
	if (cmd != V2D_IOCTL_SET_DIMENSIONS) return -ENOTTY;
	
	if ((ret = mutex_lock_interruptible(&context->ioctl_lock)) != 0) return ret;
	
		if ((ret = mutex_lock_interruptible(&context->lock)) != 0) return ret;
		
		if (context->canvas != NULL) {
			mutex_unlock(&context->lock);
			mutex_unlock(&context->ioctl_lock);
			return -EINVAL;
		}
		
		mutex_unlock(&context->lock);
	
	// verify dimensions
	struct v2d_ioctl_set_dimensions dim;
	if (copy_from_user(&dim, (void *)arg, sizeof(dim)) != 0) {
		mutex_unlock(&context->ioctl_lock);
		return -EFAULT;
	}
	if (dim.height <= 0 || V2D_MAX_HEIGHT < dim.height || dim.width <= 0 || V2D_MAX_WIDTH < dim.width) {
		mutex_unlock(&context->ioctl_lock);
		return -EINVAL;
	}
	if (DEBUG) printk(KERN_DEBUG "v2d_chrdev: ioctl: height %d, width %d\n", dim.height, dim.width);
	
	// alloc canvas
	v2d_canvas_t * canvas = v2d_canvas_alloc(dim, &v2d_pci_get_device(context->device)->dev);
	if (canvas == NULL) {
		mutex_unlock(&context->ioctl_lock);
		return -ENOMEM;
	}
	
		mutex_lock(&context->lock);
		context->canvas = canvas;
		mutex_unlock(&context->lock);
	
	mutex_unlock(&context->ioctl_lock);
	
	return 0;
}

static int v2d_mmap (struct file * file, struct vm_area_struct * mem) {
	if (DEBUG) printk(KERN_DEBUG "v2d_chrdev: mmap\n");
	
	v2d_context_t * context = file->private_data;
	if (context == NULL) return -EFAULT;
	
	int ret;
	if ((ret = mutex_lock_interruptible(&context->lock)) != 0) {
		return ret;
	}
	
	if (context->canvas == NULL) {
		mutex_unlock(&context->lock);
		return -EINVAL;
	}
	v2d_canvas_t * canvas = context->canvas;
	
	// check size
	size_t size = mem->vm_end - mem->vm_start;
	if (size > V2D_CANVAS_PAGE_COUNT(
		canvas->dim.width,
		canvas->dim.height
	) * PAGE_SIZE) {
		mutex_unlock(&context->lock);
		return -EINVAL;
	}
	
	for (size_t page_off = 0; page_off < size / PAGE_SIZE; page_off ++) {
		//printk(KERN_DEBUG "mapping page %d, kern %p, user %p, pfn %p\n",
		//	page_off, canvas->pages[page_off], mem->vm_start + page_off * PAGE_SIZE,
		//	((unsigned int)canvas->pages[page_off]) >> PAGE_SHIFT);
		if (remap_pfn_range(
			mem,
			mem->vm_start + page_off * PAGE_SIZE,
			virt_to_phys(canvas->pages[page_off]) >> PAGE_SHIFT,
			PAGE_SIZE,
			mem->vm_page_prot
		) != 0) {
			mutex_unlock(&context->lock);
			return -EFAULT;
		}
	}
	
	mutex_unlock(&context->lock);
	return 0;
}

static int v2d_fsync (struct file * file, loff_t a, loff_t b, int datasync) {
	if (KERN_DEBUG) printk(KERN_DEBUG "v2d_chrdev: fsync\n");
	v2d_context_t * context = file->private_data;
	if (context == NULL) return -EFAULT;
	
	int ret;
	if ((ret = mutex_lock_interruptible(&context->lock)) != 0) {
		return ret;
	}
	
	if (context->device == NULL) {
		mutex_unlock(&context->lock);
		return -EFAULT;
	}
	if (context->canvas == NULL) {
		mutex_unlock(&context->lock);
		return -EINVAL;
	}
	
	if (!v2d_pci_sync(context->device)) {
		mutex_unlock(&context->lock);
		return -EINTR;
	}
	
	mutex_unlock(&context->lock);
	
	return 0;
}

static struct file_operations v2d_fops = {
	owner: THIS_MODULE,
	open: v2d_open,
	release: v2d_release,
	write: v2d_write,
	compat_ioctl: v2d_ioctl,
	unlocked_ioctl: v2d_ioctl,
	mmap: v2d_mmap,
	fsync: v2d_fsync,
};

int v2d_chrdev_major = 0;

struct class * v2d_chrdev_class = NULL;

bool v2d_chrdev_init(void) {
	if (DEBUG) printk(KERN_DEBUG "v2d_chrdev: init\n");
	
	int major = register_chrdev(0, CHRDEV_NAME, &v2d_fops);
	if (major <= 0) {
		v2d_chrdev_major = 0;
		printk(KERN_ERR "v2d_chrdev: register_chrdev failed\n");
		return false;
	} else {
		v2d_chrdev_major = major;
	}
	
	v2d_chrdev_class = class_create(THIS_MODULE, "v2d_chrdev_class");
	if (v2d_chrdev_class == NULL) {
		printk(KERN_ERR "v2d_chrdev: failed to register device class\n");
		return false;
	}
	
	return true;
}

void v2d_chrdev_cleanup(void) {
	if (DEBUG) printk(KERN_DEBUG "v2d_chrdev: cleanup\n");
	
	if (v2d_chrdev_class != NULL) {
		class_destroy(v2d_chrdev_class);
		v2d_chrdev_class = NULL;
	}
	
	if (v2d_chrdev_major > 0) {
		unregister_chrdev(v2d_chrdev_major, CHRDEV_NAME);
	}
	v2d_chrdev_major = 0;
}
