#ifndef V2D_CHRDEV_H
#define V2D_CHRDEV_H

#include <linux/types.h>
#include <linux/list.h>

typedef struct _v2d_context_t v2d_context_t;

extern int v2d_chrdev_major;
extern struct class * v2d_chrdev_class;

extern void v2d_chrdev_finalize_context(struct list_head * context);

extern bool v2d_chrdev_init(void);
extern void v2d_chrdev_cleanup(void);

#endif
